<?php

use App\Http\Controllers\productController;
use App\Http\Controllers\OTPController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('product',[productController::class,'indexProduct']); // list product //
Route::get('product/{id}',[productController::class,'showProduct']); // detail product //
Route::post('product',[productController::class,'storeProduct']); // add product //
Route::put('product',[productController::class,'updateProduct']); // edit product //
Route::delete('product/{id}',[productController::class,'destroyProduct']); // delete product //
Route::post('book',[productController::class,'storebook']); // insert data with image //
Route::get('book',[productController::class,'indexbook']); // list book //

Route::get('dynamic',[productController::class,'getDynamic']); // list dynamic field //
Route::put('stockIn',[productController::class,'updateQty']); // list dynamic field //
Route::put('stockOut',[productController::class,'updateQtyOut']); // list dynamic field //
Route::post('sendOtp', [OTPController::class, 'generateOTP']);