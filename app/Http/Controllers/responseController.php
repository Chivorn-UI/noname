<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class responseController extends Controller
{
    // class reponse  success
    public static function storeProDel($id){

        return response()->json(
            [
                'status' => 200,
                'result'=> [['insertProId' => $id]]
            ],Response::HTTP_OK);
    }
    // class reponse  success
    public static function success($result){

        return response()->json(
        [
            'status' => 200,
            'result'=> $result
        ],200);
    }
    //mobile
    public static function sendResponseSuccessMobile($result, $message)
    {
    	if(empty($result)){
            return response()->json(
                [
                    'status' => 200,
                    'message' => $message,
                ],Response::HTTP_OK);
        }
        return response()->json(
            [
                'status' => 200,
                'message' => $message,
                'result'=> $result
            ],Response::HTTP_OK);
    }
    // class reponse  success
    public static function updateSuccess($id){

        return response()->json(
            [
                'status' => 200,
                'message' => "Update Successfully",
                'result'=> [['updatedId' => $id]]
            ],Response::HTTP_OK);
    }

    // class reponse  success
    public static function insertSuccess($id){
        if(empty($id)){
            return response()->json(
                [
                    'status' => 200,
                    'message' => "Insert Successfully",
                ],Response::HTTP_OK);
        }
        return response()->json(
            [
                'status' => 200,
                'message' => "Insert Successfully",
                'result'=> [['insertLastId' => $id]]
            ],Response::HTTP_OK);
    }


    // class reponse  client error
    public static function client($result){

        return response()->json(
        [
            'status' => 400,
            'result'=> $result
        ],400);
    }
    // class reponse  client error
    public static function clientMobile($result,$message){

        return response()->json(
        [
            'status' => 400,
            'message' => $message,
            'result'=> $result
        ],400);
    }
    // class reponse  code  error
    public static function error($result){

        return response()->json(
        [
            'status' => 500,
            'result'=> $result
        ],500);
    }

    //internal error
    public static function internalError($message,$string){
        return response()->json(
            [
                'status' => 500,
                'result'=> $message,
                'string' => $string,
            ],500);
    }

    //ID not found
    public static function isHaveId ($id) {
        return response()->json(
            [
                'status' => 404,
                'message' => "ID ".$id." Not Found !"
            ],
            404
        );
    }

     //ID Already have
     public static function isIdAlreadyHave ($id) {
        return response()->json(
            [
                'status' => 404,
                'message' => "ID ".$id." Is Already have in table !"
            ],
            404
        );
    }

    // already taken
    public static function alreadyTaken($id){
        return response()->json(
            [
                'status' => 404,
                'message' => "ID ".$id." has already been taken !"
            ],
            404
        );
    }

    // ============= Delete Data success ===============
    public static function deleteSuccess ($id) {
        return \response()->json(
            [
                "status" => 200,
                "message" => "Deleted Id ".$id." Successfully",
            ],
            Response::HTTP_OK
        );
    }
    // data not found
    public static function  dataNotFound () {
        return \response()->json(
            [
                "status" => 404,
                "message" => "Data Not Found",
            ],
            Response::HTTP_NOT_FOUND
        );
    }

    //404 status
    public static function  noDataFound ($result) {
        return \response()->json(
            [
                "status" => 404,
                "result" => $result,
                "message" => "Data Not Found",
            ],
            Response::HTTP_NOT_FOUND
        );
    }

    // check banned
    public static function checkBanned(){
        return \response()->json(
            [
                "success" => 0,
                "status" => 403,
                "message" => "Your account has been temporarily suspended",
            ],
            Response::HTTP_FORBIDDEN
        );
    }

    // no permission
    public static function noPermission(){
        return \response()->json(
            [
                "status" => 403,
                "result" => [
                    "message" => "You don't have permission to access",
                ]
            ],
            Response::HTTP_FORBIDDEN
        );
    }

    // class reponse not found
    public static function notFound($field){

        return response()->json(
            [
                'status' => 404,
                'result'=> [
                    "error" => $field.' is not found'
                ]
            ],
            404
        );
    }

    //class reponse  code  error when detail data
    public static function errorDetail($result){

        $data=array(['error'=>'ID Not Found : '.$result]);
        return response()->json(
            [
                'status' => 400,
                'result'=> $data
            ],400
        );
    }

}
