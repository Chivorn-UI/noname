<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Twilio\Rest\Client;

class OTPController extends Controller
{
    public function generateOTP(Request $request)
    {
        $phoneNumber = $request -> phoneNumber; // Replace with user's phone number
        $otp = Str::random(6); // Generate a 6-digit OTP

        // Save the OTP in the session for verification
        session(['otp' => $otp]);
        session(['phoneNumber' => $phoneNumber]);
        
        $this->sendOTP($phoneNumber, $otp);

        return response()->json(['message' => 'OTP sent successfully']);
    }

    public function verifyOTP(Request $request)
    {
        $otp = $request->input('otp');
        $storedOTP = session('otp');
        $phoneNumber = session('phoneNumber');

        // Verify if the provided OTP matches the one sent to the user
        if ($otp === $storedOTP) {
            return response()->json(['message' => 'OTP verified successfully']);
        } else {
            return response()->json(['error' => 'Invalid OTP'], 400);
        }
    }

    private function sendOTP($phoneNumber, $otp)
    {
        $sid = 'AC22c453f39c77dd37a97b972044984402';
        $token = '2c95d23d304fa44366c9b7d7e27cd1f0';
        $twilioPhoneNumber ='+85586826374';

        $twilio = new Client($sid, $token);
        dd($twilio);
        $message = $twilio->messages->create(
            $phoneNumber,
            array(
                'from' => $twilioPhoneNumber,
                'body' => "Your OTP is: $otp"
            )
        );

        // No need to save the OTP in the database, as we're using session for now
    }
    public function createVerifyService()
    {
        // Find your Account SID and Auth Token at twilio.com/console
        // and set the environment variables. See http://twil.io/secure
        $sid = "AC22c453f39c77dd37a97b972044984402";
        $token = "2c95d23d304fa44366c9b7d7e27cd1f0";
        
        // Initialize Twilio Client
        $twilio = new Client($sid, $token);

        try {
            // Create Verify Service
            $service = $twilio->verify->v2->services->create("My First Verify Service");

            // Output Service SID
            return $service->sid;
        } catch (\Exception $e) {
            // Handle exceptions
            return 'Error: ' . $e->getMessage();
        }
    }



}
