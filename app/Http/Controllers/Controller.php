<?php

namespace App\Http\Controllers;

use App\Http\Controllers\responseController;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

/** @OA\Info(title="POS Documentation", version="0.1") */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    //=========================== check user id =====================
    public static function isHaveIdInTable($id, $tableName, $condition = "")
    {
        DB::beginTransaction();
        try {
            $result = DB::select("SELECT id FROM {$tableName} WHERE id=? AND is_deleted = 0 {$condition}", [$id]);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return responseController::error($ex->getMessage());
        }
        return $result;
    }

    public static function isHaveShopIdInTable($id, $tableName, $condition = "")
    {
        DB::beginTransaction();
        try {
            $result = DB::select("SELECT shop_id FROM {$tableName} WHERE shop_id=? AND is_deleted = 0 {$condition}", [$id]);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return responseController::error($ex->getMessage());
        }
        return $result;
    }

    public static function isHaveIdInTableProduct($id, $tableName, $condition = "")
    {
        DB::beginTransaction();
        try {
            $result = DB::select("SELECT id FROM {$tableName} WHERE id=? AND status = 'ENABLE' {$condition}", [$id]);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return responseController::error($ex->getMessage());
        }
        return $result;
    }

    public static function notificationByUserMobile($userId, $title, $body)
    {
        $firebaseToken = DB::select('SELECT notification_token FROM users WHERE id = ?', [$userId]);
        if (!empty($firebaseToken)) {
            $firebaseToken = $firebaseToken[0]->notification_token ?? '';
        } else {
            $firebaseToken = '';
        }
        $SERVER_API_KEY = env('FCM_SERVER_KEY');
        $data = [
            "registration_ids" => [$firebaseToken],
            "notification" => [
                "title" => $title,
                "body" => $body,
            ]
        ];
        $dataString = json_encode($data);

        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        return curl_exec($ch);
    }
    public static function pushNotificationToMultiDevice($firebaseToken, $title, $body, $imageUrl)
    {
        // dd($firebaseToken);
        $SERVER_API_KEY = env('FCM_SERVER_KEY');
        $data = [
            "registration_ids" => $firebaseToken,
            "notification" => [
                "title" => $title,
                "body" => $body,
                'image' =>  url('/') . '/' . $imageUrl
            ]
        ];
        $dataString = json_encode($data);

        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        return curl_exec($ch);
    }
}
