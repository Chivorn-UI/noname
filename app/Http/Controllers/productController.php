<?php

namespace App\Http\Controllers;

use App\Http\Controllers\responseController;
use App\Models\productModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class productController extends Controller
{   
    // List Product
    public function indexProduct(Request $request){
        $page = $request -> page ?? null;
        $perPage =$request -> perPage ?? null;
        return  productModel::indexDataProduct($page,$perPage);
    }
    // List dynamic
    public function getDynamic(Request $request){
        $fields =$request -> fields ?? null;
        return  productModel::indexDataDynamic($fields);
    }
    // Detail Products
    public function showProduct($id){
        try {
            $tableName = "product";
            $isHaveId = Controller::isHaveIdInTable($id, $tableName);
            if (empty($isHaveId)) {
                return responseController::isHaveId($id);
            }
            return responseController::success(productModel::showDataProduct($id));
        } catch (\Exception $ex) {
            return responseController::error($ex->getMessage());
        }
    }
    // Add Product 
    public function StoreProduct(Request $request){
        $categoryId     = $request -> categoryId;
        $price          = $request -> price;
        $release_date   = $request -> release_date;
        $description    = $request -> description;
        $product_name   = $request -> product_name;
        $validator = Validator::make(
            $request->all(),
            [
                'categoryId' => 'required',
                'price' => 'required',
                'release_date' => 'required',
                'description' => 'required',
                'product_name' => 'required',
            ],
        );
        if ($validator->fails()) {
            return responseController::client($validator->getMessageBag());
        }
        return  productModel::StoreDataProduct($categoryId,$price,$release_date,$description,$product_name);
    }

    // update product //
    public function updateProduct (Request $request){
        try {
            $id             = $request-> id;
            $categoryId     = $request -> categoryId;
            $price          = $request -> price;
            $release_date   = $request -> release_date;
            $description    = $request -> description;
            $product_name   = $request -> product_name;
            $tableName = "product";
            $isHaveId = Controller::isHaveIdInTable($request->id, $tableName);
            if (empty($isHaveId)) {
                return responseController::isHaveId($request->id);
            }
            $result = productModel::updateDataProduct($categoryId,$price,$release_date,$description,$product_name,$id);
            return $result;
        } catch (\Exception $ex) {
            return responseController::error($ex->getMessage());
        }
    }

    // Delete product 
    public function destroyproduct($id)
    {
        try
        {
            $tableName = "product";
            $isHaveId = Controller::isHaveIdInTable($id, $tableName);
            if (empty($isHaveId)) {
                $result = responseController::isHaveId($id);
                return $result;
            }
            $result = productModel::destroyDataproduct($id);
            return $result;
        } catch (\Exception $ex) {
            return responseController::error($ex->getMessage());
        }
    }
    public function storeBook(Request $request){
        try
        {
            if (isset($request->file))
            {
                $files = $request->file('file');
                $fileInfo = [];
                $num = 0;

                foreach($files as $file)
                {
                    $fileName = time().md5(time()).$num.'.'.$file->getClientOriginalExtension();
                    // Get extensions
                    $fileInfo[] = [
                        'fileName'      => $file->getClientOriginalName(),
                        'pathFile'      => $file->getPathname(),
                        'type'          => $file->getMimeType(),
                        'name'          => time().md5(time()).$num.'.'.$file->getClientOriginalExtension(),
                        'extension'     => $file->getClientOriginalExtension(),
                        'path'          => 'media/file/crm/uploadfile/'.$fileName,
                    ];
                    // Move file to directory => media/file/crm/uploadfile
                    $file->move(public_path().'/media/file/crm/uploadfile/', $fileName);
                    $num++;
                }
            }
            $image = $fileInfo[0]['path'];

            $title = $request -> title;
            $author = $request -> author;

            return productModel::storeDataBook($title,$author,$image);

        } catch (\Exception $ex) {
            return responseController::error($ex->getMessage());
        }
    }
    public function indexbook(Request $request){
        $page = $request -> page ?? null;
        $perPage =$request -> perPage ?? null;
        return  productModel::indexDatabook($page,$perPage);
    }

    public function updateQty(Request $request){
        $productId     = $request -> productId;
        $qty           = $request -> qty;
        $validator = Validator::make(
            $request->all(),
            [
                'productId' => 'required',
                'qty'       => 'required',
            ],
        );
        if ($validator->fails()) {
            return responseController::client($validator->getMessageBag());
        }

        $check = DB::select("SELECT qty FROM product WHERE id = ?",[$productId]);
        $oldQty = $check[0]->qty;
        $proQty = $oldQty + $qty ;
        return  productModel::updateQtyProduct($productId,$proQty);
    }
    public function updateQtyOut(Request $request){
        $productId     = $request -> productId;
        $qty           = $request -> qty;
        $validator = Validator::make(
            $request->all(),
            [
                'productId' => 'required',
                'qty'       => 'required',
            ],
        );
        if ($validator->fails()) {
            return responseController::client($validator->getMessageBag());
        }

        $check = DB::select("SELECT qty FROM product WHERE id = ?",[$productId]);
        $oldQty = $check[0]->qty;
        $proQty = $oldQty - $qty ;
        return  productModel::updateQtyProduct($productId,$proQty);
    }
}
