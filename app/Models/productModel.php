<?php

namespace App\Models;

use App\Http\Controllers\responseController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\paginationModel;
use Illuminate\Support\Facades\DB;

class productModel extends Model
{
    public static function indexDataProduct($page,$perPage){
        DB::beginTransaction();
        try{
            $result ="SELECT * FROM category";
            DB::commit();
            return paginationModel::showPage($result,$perPage,'','','',$page,'');
        }    catch (\Exception $ex) {
            DB::rollBack();
            return responseController::error($ex->getMessage());
        }

    }
    public static function indexDataDynamic($fields)
    {
        DB::beginTransaction();

        try {
      
            // Define the default fields to retrieve
            $defaultFields = [
                'id',
                'category_id', 
                'name', 
                'name_kh', 
                'type', 
                'image',
                'price', 
                'status', 
                'description',
                'expiration_date',
                'created_at', 
                'updated_at', 
                'shop_id', 
                'currency',
                'barcode',
                'cost', 
                'qty',
                'amount', 
                'image_thumbnail', 
                'filename', 
                'pathfile', 
                'path', 
                'currency_id', 
                'unit_price', 
                'discount', 
                'discount_type', 
                'taxation', 
                'code', 
                'code_prefix_owner_id', 
                'measurement_id', 
                'choice_options', 
                'brand',
                'vendor_id', 
                'last_stock_purchase', 
                'last_sold_date', 
                'gp', 
                'item_code'
            ];
            
            $selectedFields = empty($fields) ? $defaultFields : explode(',', $fields);
            
            $validatedFields = array_intersect($defaultFields, $selectedFields);

            $fieldsString = implode(', ', $validatedFields);
            // // check field that don't match with field in list //  
            if (count(array_diff($selectedFields, $defaultFields)) > 0) {
                // If there are fields that are not in the default list, return an error message
                return responseController::error("Invalid fields. Available fields are only : " . implode(' , ', $defaultFields));
            
            }
            $result = DB::select("SELECT {$fieldsString} FROM product_by_categories WHERE status != 'DELETE' ");

            DB::commit();
            return responseController::success($result);
        } catch (\Exception $ex) {
            DB::rollBack();

            return responseController::error($ex->getMessage());
        }
    }



    public static function showDataProduct($id){
        DB::beginTransaction();
        try{
            $result =DB::select("SELECT p.product_name,
                                        c.name as category_name,
                                        p.price,
                                        p.description
                                FROM    product p
                                LEFT JOIN category c on c.id = p.category_id
                                WHERE p.id = $id ");
            DB::commit();
            return $result ;
        }    catch (\Exception $ex) {
            DB::rollBack();
            return responseController::error($ex->getMessage());
        }
    }

    public static function StoreDataProduct($categoryId,$price,$release_date,$description,$product_name){
        DB::beginTransaction();
            try {
                $result = DB::select("SELECT insert_product(?,?,?,?,?) ",[$categoryId,$price,$release_date,$description,$product_name]);
                DB::commit();
    
                $result = array_map(function ($value) {
                    return (array)$value;
                }, $result);
    
                $menuId = $result[0]['insert_product(?,?,?,?,?)'];
                return responseController::insertSuccess($menuId);
            } catch (\Exception $ex) {
                DB::rollBack();
                return responseController::success($ex->getMessage());
            }
        }
    public static function updateDataProduct($categoryId,$price,$release_date,$description,$product_name,$id){
        DB::beginTransaction();
            try {
                
                $result = DB::select("SELECT update_product(?,?,?,?,?,?)",
                [
                    $categoryId,$price,$release_date,$description,$product_name,$id
                ]);
                DB::commit();
    
                $result = array_map(function ($value) {
                    return (array)$value;
                }, $result);
    
                $productId = $result[0]['update_product(?,?,?,?,?,?)'];
                return responseController::updateSuccess($productId);
            } catch (\Exception $ex) {
                DB::rollBack();
                return responseController::success($ex->getMessage());
            }
    }
    public static function destroyDataproduct($id){
        DB::beginTransaction();
        try {
            $result = DB::select("SELECT delete_product(?)", [$id]);
            DB::commit();
            $result = array_map(function ($value) {
                return (array)$value;
            }, $result);

            $getLastId = $result[0]['delete_product(?)'];
            return responseController::deleteSuccess($getLastId);

        } catch (\Exception $ex) {
            DB::rollBack();
            return responseController::error($ex->getMessage());
        }
    }
    public static function storeDataBook($title,$author,$image){
        DB::beginTransaction();
        try {

            $transactionNum = DB::select("SELECT gen_book_no_prefix()");
                $tranNum = array_map(function ($value) {
                    return (array)$value;
                }, $transactionNum);
            $getNo= $tranNum[0]['gen_book_no_prefix()'];

            $result = DB::select("SELECT insert_book(?,?,?,?)", [$getNo,$title,$author,$image]);

            DB::commit();
            $result = array_map(function ($value) {
                return (array)$value;
            }, $result);

            $getLastId = $result[0]['insert_book(?,?,?,?)'];
            return responseController::insertSuccess($getLastId);

        } catch (\Exception $ex) {
            DB::rollBack();
            return responseController::error($ex->getMessage());
        }
    }
    public static function indexDatabook($page,$perPage){
        DB::beginTransaction();
        try{
            $result =" SELECT * FROM roles
                            -- CONCAT('B-', LPAD(b.no, 8, 0)) as bookNo
                            -- ,title
                            -- ,author
                            -- ,source
                            -- from book b
                    ";
            DB::commit();
            return paginationModel::showPage($result,$perPage,'','','',$page,'');
        }    catch (\Exception $ex) {
            DB::rollBack();
            return responseController::error($ex->getMessage());
        }
    }
    public static function updateQtyProduct($productId,$proQty){
        DB::beginTransaction();
            try {
                
                $result = DB::select("SELECT update_qty(?,?)",[$productId,$proQty]);
                DB::commit();
    
                $result = array_map(function ($value) {
                    return (array)$value;
                }, $result);
    
                $productId = $result[0]['update_qty(?,?)'];
                return responseController::updateSuccess($productId);
            } catch (\Exception $ex) {
                DB::rollBack();
                return responseController::success($ex->getMessage());
            }
    }
}
